package data;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import model.Student;


@SessionScoped
@Named
public class StudentListProducer implements Serializable{

	private static final long serialVersionUID = -182866064791747156L;
	
	private List<Student> students;
	
	public StudentListProducer() {
		students = createMockStudents();
	}
	
	public List<Student> getStudents(){
		return students;
	}
	
	public List<Student> createMockStudents(){
		
		Student student1 = new Student();
		student1.setVorname("Oliver");
		student1.setNachname("Guder");
		student1.setStrasse("FranzXaver 3");
		student1.setPostleitzahl(6372);
		student1.setOrt("Oberndorf");
		
		Student student2 = new Student();
		student2.setVorname("Daniel");
		student2.setNachname("Ertl");
		student2.setStrasse("Gassenweg 4");
		student2.setPostleitzahl(6372);
		student2.setOrt("Piesendorf");
		
		
		List<Student> ret = new LinkedList<>();
		ret.add(student1);
		ret.add(student2);
		return ret;
		
	}
}
