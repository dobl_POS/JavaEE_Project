package data;


import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import model.Student;

import java.io.Serializable;

@SessionScoped
public class StudentProducer implements Serializable {

    private static final long serialVersionUID = -1828660647917534556L;

    private Student student;
    
    @Produces
    @Named
    public Student getSelectedStudent() {
        return student;
    }

    public void setSelectedStudent(Student student) {
        this.student = student;
    }
}
