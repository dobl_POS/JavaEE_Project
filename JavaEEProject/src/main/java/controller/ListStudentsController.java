package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import model.Student;

@SessionScoped
@Named
public class ListStudentsController implements Serializable {

	private static final long serialVersionUID = 8693277383648025822L;
	
	
	public String doAddStudents() {
		
		return Pages.EDIT_STUDENTS;
	}
	
	public String doListStudents(Student student) {
		System.out.println("List student " + student);
		return Pages.LIST_STUDENTS;
	}
}
