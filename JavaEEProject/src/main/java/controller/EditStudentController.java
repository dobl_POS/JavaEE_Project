package controller;


import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import data.StudentProducer;
import model.Student;
import util.Events.Added;
import util.Events.Updated;

import java.io.Serializable;

@ViewScoped
@Named
public class EditStudentController implements Serializable {
    private static final long serialVersionUID = 2815796004558360299L;

    @Inject
    @Added
    private Event<Student> studentAddEvent;

    @Inject
    @Updated
    private Event<Student> studentsUpdateEvent;

    @Inject
    private StudentProducer studentProducer;

    public String doSave() {
        studentAddEvent.fire(studentProducer.getSelectedStudent());

        return Pages.INDEX;
    }

    public String doCancel() {
        return Pages.INDEX;
    }

}
