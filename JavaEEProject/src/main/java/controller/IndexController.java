package controller;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@ViewScoped
@Named
public class IndexController implements Serializable {
	
	//private static final long serialVersionUID = 8693277345678025822L;
	

	
    public String doListStudents() {
        return Pages.LIST_STUDENTS;
    }

    public String doAddStudent() {
        return Pages.EDIT_STUDENTS;
    }
    
}
