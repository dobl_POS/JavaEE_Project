package model;

import java.util.Calendar;

public class Student {

	private Long matrikelnummer;
	private String nachname;
	private String vorname;
	private String strasse;
	private int postleitzahl;
	private String ort;
	
	private static int number = 0;
	
	public Student() {
		this.matrikelnummer = generateMatrikelnummer();
		this.nachname = null;
		this.vorname = null;
		this.strasse = null;
		this.postleitzahl = 0;
		this.ort = null;
	}
	
	public Student(String nachname, String vorname, String strasse, int postleitzahl, String ort) {
		super();
		
		this.matrikelnummer = generateMatrikelnummer();
		this.nachname = nachname;
		this.vorname = vorname;
		this.strasse = strasse;
		this.postleitzahl = postleitzahl;
		this.ort = ort;
	}
	
	private Long generateMatrikelnummer() {
		String jahrS = ""+Calendar.YEAR;
		int jahr = Integer.parseInt(jahrS.substring(2));
		Long matrikelnummer = Long.parseLong(jahr + "30" + number);
		number++;
		return matrikelnummer;
	}

	public Long getMatrikelnummer() {
		return matrikelnummer;
	}

	public void setMatrikelnummer(Long matrikelnummer) {
		this.matrikelnummer = matrikelnummer;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public int getPostleitzahl() {
		return postleitzahl;
	}

	public void setPostleitzahl(int postleitzahl) {
		this.postleitzahl = postleitzahl;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}
	
	
	
}
